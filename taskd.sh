#!/bin/sh -e

DIRNAME=`dirname $0`
cd $DIRNAME
USAGE="$0 [ --update ]"
if [ `id -u` != 0 ]; then
echo 'You must be root to run this script'
exit 1
fi

echo "Building and Loading Policy"
set -x
make -f /usr/share/selinux/devel/Makefile taskd.pp || exit
/usr/sbin/semodule -i taskd.pp

# Generate a man page off the installed module
sepolicy manpage -p . -d taskd_t
